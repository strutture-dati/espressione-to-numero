//
// Created by Alessandro on 06/03/2020.
//

#ifndef COMPITO_ALEMANNO_LISTA_H
#define COMPITO_ALEMANNO_LISTA_H

#include <cstdlib>
#include <iostream>
using namespace std;
struct nodo  //STruttura per creazione nodo
{
    int numero;
    struct nodo *next;

};



void inserimentocodalista(struct nodo* &testa, int num) //Questa funzione inserisce un nodo in coda alla lista; I parametri son passati per riferimento perchè verranno modificati
{
    struct nodo* nuovo;
    struct nodo* scorri;
   nuovo=new struct nodo;
   nuovo->numero=num;
    if (testa == NULL)
    {
        nuovo->next=testa;
        testa=nuovo;
    }
    else
    {
        scorri = testa;
        while (scorri->next != NULL)
        {
            scorri=scorri->next;
        }
        nuovo->next=scorri->next;
        scorri->next=nuovo;
    }

}

void stampalista(struct nodo* testa){ // Funzione per la stampa della lista con all'interno i numeri estratti; I parametri son passati per valore perchè all'interno non vengono modificati
    struct nodo* scorri;
    scorri=testa;
    cout<<"I numeri estratti e inseriti nella lista sono:"<<endl;
    while (scorri != NULL){
        cout<<scorri->numero<<endl;
        scorri=scorri->next;
    }


}
#endif //COMPITO_ALEMANNO_LISTA_H
