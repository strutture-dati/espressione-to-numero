//
// Created by Alessandro on 06/03/2020.
//

#ifndef COMPITO_ALEMANNO_ALBERO_H
#define COMPITO_ALEMANNO_ALBERO_H
#include<stdlib.h>
#include <iostream>
using namespace std;
struct radice   //Struttura per la creazione delle radici dell'albero
{
    int numero;
    struct radice* sx;
    struct radice* dx;
};
struct radice* crearadice(int n) {  //Questa funzione crea e restituisce una radice;
    struct radice* nuovo=new struct radice;
    nuovo->numero = n;
    nuovo->sx = NULL;
    nuovo->dx = NULL;
    return nuovo;
}



struct radice* inserisci_albero(struct radice* root, int n) //Questa funzione inserisce le radici nell'albero in maniera ordinata(I piccoli a sx e i grandi a dx); I parametri son passati per valore perchè successivamente vengono restituiti
{
    if (root == NULL) return crearadice(n);
    if (n < root->numero)
        root->sx  = inserisci_albero(root->sx, n);
    else if (n > root->numero)
        root->dx = inserisci_albero(root->dx, n);

    return root;
}



void stampa_albero_srd(struct radice* root) {//Questa funzione stampa l'albero, con all'interno i numeri estratti, e li stampa seguendo la regola inorder(Prima a sinistra, poi la radice, poi destra)
    if(root == NULL) return;
    stampa_albero_srd(root->sx);
    cout<<root->numero<<endl;
    stampa_albero_srd(root->dx);
}





#endif //COMPITO_ALEMANNO_ALBERO_H
