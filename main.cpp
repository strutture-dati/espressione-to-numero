#include <iostream>
#include <cstdlib>     /* atoi */
#include <vector>
#include "lista.h" //Header contenente le funzioni per la gestione della lista
#include <list>
#include <stack>
#include "albero.h"// Header contenente le funzioni per la gestione degli alberi
using namespace std;
void espressione_numero_array(int numeri[], char s[],int &k);//Questa funzione data un espressione numerica passata tramite una stringa mi restituisce i numeri contenuti all'interno tramite un array di interi; I parametri sono entrambi passati per riferimento essendo degli array; La variabile K viene passata per riferimento perchè mi restituirà l'ultima posizione utile ai fini della stampa
void stampa_array(int numeri[],int k); // Questa funzione stampa l'array con all'interno i numeri estratti;
void espressione_numero_vector(vector <int> &num, char s[]); //Questa funzione estrae i dati dall'espressione e li inserisce all'interno di un vector; I parametri son passati per riferimento perchè all'interno verranno modificati.
void stampa_vector(vector<int> num);//Questa funzione stampa il vector con i numeri estratti; I parametri son passati per valore perchè non vengono modificati all'interno
void espressione_numero_lista(struct nodo* &testa, char s[]);// Questa funzione estrae i dati dall'espressione e li inserisce all'interno di una lista; I parametri son passati per riferimento perchè all'interno verranno modificati
void espressione_numero_classlist(list<int> &nume, char *s); // Questa funzione strae i dati dall'espressione e li inserisce all'interno di una lista creata con la classe list; I parametri son passati per riferimento perchè all'interno verrano modificati
void stampa_classlist(list <int> nume);//Questa funzione stampa la lista creata con la classe list contenente i numeri estratti; I parametri son passati per valore perchè non vengono modificati all'interno
void espressione_numero_stack(stack <int> &number,char*s);//Questa funzione estrae i dati dall'espressione e li inserisce all'interno di uno stack;I parametri son passati per riferimento perchè all'interno vengono modificati
void stampa_stack(stack <int> number);//Questa funzione stampa lo stack contenente i numeri estratti; I parametri son passati per valore perchè non vengono modificati all'interno
void espressione_numero_albero(struct radice* &root,char*s);//Questa funzione estrae i dati dall'espressione e li inserisce all'interno di un albero;I parametri son passati per riferimento perchè all'interno vengono modificati
int main()
{
    int k;
    vector <int> num;
    list <int> nume;
    stack<int> number;
    int numeri[100];
    struct nodo* testa=NULL;
    struct radice* root=NULL;
    char s[200];

    cout<<"Dammi l'espressione:";
    cin>>s;
    cout<<endl;
    cout<<"Espressione iniziale:"<<s<<endl;
    cout<<endl;
    espressione_numero_array(numeri,s,k);
    stampa_array(numeri,k);
    cout<<endl;
    espressione_numero_vector(num,s);
    stampa_vector(num);
    cout<<endl;
    espressione_numero_lista(testa,s);
    stampalista(testa);
    cout<<endl;
    espressione_numero_classlist(nume,s);
    stampa_classlist(nume);
    cout<<endl;
    espressione_numero_stack(number,s);
    stampa_stack(number);
    cout<<endl;

    espressione_numero_albero(root,s);
    cout<<"Gli elementi estratti e inseriti all'interno dell'albero sono:"<<endl;
    stampa_albero_srd(root);
    return 0;
}


void espressione_numero_array(int numeri[], char s[], int &k){
    char s1[25]="";
    int i=0;
    int j=0;

    for(k=0;k<100;k++) {
        numeri[k] = -1;
    }
    k=0;

    while(s[i]!='\0'){
        j=0;
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                numeri[k]=atoi(s1);
                k++;
            }

        }
        i++;
    }
}

void stampa_array(int numeri[],int k){
    cout<<"I valori estratti sono:"<<endl;
    for (int i = 0; i < k ; ++i)
        cout<<numeri[i]<<endl;


}

void espressione_numero_vector(vector<int> &num, char s[]){
    char s1[25]="";
    int i=0;
    int j=0;



    while(s[i]!='\0'){
        j=0;
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                num.push_back(atoi(s1));

            }

        }
        i++;
    }
}

void stampa_vector(vector<int> num){
    vector<int>::iterator i;
    cout<<"I numeri estratti e inseriti nel vector sono:"<<endl;
    for (i = num.begin(); i < num.end() ; ++i) {
        cout<<*i<<endl;
    }
}

void espressione_numero_lista(struct nodo* &testa, char s[]){
    char s1[25]="";
    int i=0;
    int j=0;



    while(s[i]!='\0'){
        j=0;
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                inserimentocodalista(testa,atoi(s1));

            }

        }
        i++;
    }
}


void espressione_numero_classlist(list<int> &nume, char *s){
    char s1[25]="";
    int i=0;
    int j=0;



    while(s[i]!='\0'){
        j=0;
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                nume.push_back(atoi(s1));

            }

        }
        i++;
    }
}

void stampa_classlist(list <int> nume){
    list <int>::iterator i;
    cout<<"I numeri estratti e inseriti all'interno della classe list sono: "<<endl;
    for (i = nume.begin(); i!=nume.end(); ++i) {
        cout<<*i<<endl;
    }
}


void espressione_numero_stack(stack <int> &number,char*s){
    char s1[25]="";
    int i=0;
    int j=0;

    while(s[i]!='\0'){
        j=0;
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                number.push(atoi(s1));

            }

        }
        i++;
    }
}
void stampa_stack(stack <int> number){
    cout<<"Gli elementi estratti ed inseriti all'interno dello stack sono:"<<endl;
    while(!number.empty()){
        cout<<number.top()<<endl;
        number.pop();
    }
}


void espressione_numero_albero(struct radice* &root,char*s){
    char s1[25]="";
    int i=0;
    int j=0;

    while(s[i]!='\0'){
        j=0;
        while(s[i]>='0'&&s[i]<='9'){
            s1[j]=s[i];
            i++;
            j++;
            if(!(s[i]>='0'&&s[i]<='9') )
            {
                s1[j]='\0';
                root=inserisci_albero(root,atoi(s1));

            }

        }
        i++;
    }
}